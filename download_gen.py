import argparse
import configparser
import dxpy
import smtplib
from email.mime.text import MIMEText
from pathlib import Path
import os

EMAIL_SMTP_HOST = 'smtp.johnshopkins.edu'
EMAIL_SENDER_ADDR = 'AllOfManifestManager@cidr.jhmi.edu'

def parse_args(args):
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('config_file', help='Required config file')
    args = parser.parse_args(args)
    return args


def main(passed_args=None):
    """
    Downloads GEN type manifest from DNAnexus to local directory and archives
    the manifest and validation files in DNAnexus. The config must be an `.ini`
    file that contains:

    [DNANEXUS]
    token = TOKEN
    workspace_id = PROJECT_ID
    root_folder = /ROOT_INTAKE_FOLDER
    """

    args = parse_args(passed_args)
    print(args.config_file)
    config = configparser.ConfigParser()
    config.read(args.config_file)
    print('Size         :', os.path.getsize(args.config_file))
    print(config.read(args.config_file))
    configure_dxpy(config['DNANEXUS'])
    download_files(config['DNANEXUS'])
    return 0


def configure_dxpy(config):
    CREDENTIALS = {
        'auth_token_type': 'Bearer',
        'auth_token': config['token']
    }
    dxpy.set_security_context(CREDENTIALS)
    dxpy.set_workspace_id(config['workspace_id'])

def generatePlainTextMessageHeader(subject, message):
    msg = MIMEText(message)
    msg['Subject'] = subject

    return msg

def sendEmailMessage(recipients, mimeMessage):
    smtp_obj = smtplib.SMTP(EMAIL_SMTP_HOST)
    smtp_obj.sendmail(EMAIL_SENDER_ADDR, recipients, mimeMessage.as_string())
    smtp_obj.quit()

def download_files(config):
    manifest_dir = config['root_folder'] + config['manifest_folder']
    archive_dir = config['root_folder'] + 'archive'
    manifest_out_dir = config['manifest_out_dir']

    print(manifest_dir)
    print(archive_dir)
    print(manifest_out_dir)

# Query for file_ids to move
    files = dxpy.bindings.search.find_data_objects(
        classname='file',
        name='*.csv',
        name_mode='glob',
        folder=manifest_dir,
        recurse=False,
        return_handler=True
    )
    # Download files
    is_empty = True
    fileNames = []
    for file in files:
        print(file)
        is_empty = False
        file_id = file.describe()['id']
        
        # Validation project
        #name = '\\\\jhgmnt.jhgenomics.jhu.edu\\research\\active\\AllofUs_BHCGC_GDA_092518_1\\Project_Management\\Sites\\Receipt\\Original_Samples\\test\\' + file.describe()['name']\
        # + file_id
        
        #PRODUCTION
        name = manifest_out_dir + file.describe()['name']
        print('Downloading ' + name + ' ' + file_id)
        
        dxpy.bindings.dxfile_functions.download_dxfile(
            dxid=file_id,
            filename=name
        )
        
        #fileNames.append(name)
        print('Archiving ' + name)
        file.move(folder=archive_dir)
        
    #if fileNames:
    #   Test Channel - Playground - Johns Hopkins Genomics <b945086a.live.johnshopkins.edu@amer.teams.ms>
    #   CIDR_AllofUs_BHCGC_P2218 - Johns Hopkins Genomics <2e988e84.live.johnshopkins.edu@amer.teams.ms>
    #   sendEmailMessage('kduerr1@jh.edu', generatePlainTextMessageHeader("All Of Us Manifest Download","\n".join(fileNames)))
        #sendEmailMessage('2e988e84.live.johnshopkins.edu@amer.teams.ms', generatePlainTextMessageHeader("All Of Us Manifest Download","\n".join(fileNames)))
    # else:
    #     sendEmailMessage('kduerr1@jh.edu', generatePlainTextMessageHeader("All Of Us Manifest Download","TESTING"))

        # print('No new manifest to archive.')


if __name__ == '__main__':
    main()

